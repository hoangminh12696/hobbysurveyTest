-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 19, 2021 at 11:36 PM
-- Server version: 10.4.20-MariaDB
-- PHP Version: 8.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hobbysurvey`
--

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `name` varchar(512) COLLATE utf8mb4_unicode_ci NOT NULL,
  `food` varchar(512) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pet` varchar(512) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `name`, `food`, `pet`) VALUES
(1, 'minh', 'chocolate', 'dog'),
(27, 'lan', 'banana', 'hamster'),
(28, 'phuong', 'chocolate', 'cat'),
(29, 'trang', 'chocolate', 'dog'),
(30, 'mai', 'banana', 'dog'),
(31, 'phong', 'chicken', 'hamster'),
(32, 'hanh', 'chicken', 'cat'),
(33, 'linh', 'chocolate', 'hamster'),
(34, 'anh', 'banana', 'cat'),
(35, 'ngoc', 'chocolate', 'dog'),
(36, 'khanh', 'banana', 'dog'),
(37, 'dat', 'banana', 'hamster'),
(38, 'diep', 'chocolate', 'cat'),
(39, 'son', 'chocolate', 'hamster'),
(40, 'duc', 'chicken', 'dog'),
(41, 'duong', 'banana', 'dog'),
(42, 'bao', 'chicken', 'hamster'),
(43, 'hoi', 'banana', 'dog'),
(44, 'la', 'chicken', 'cat'),
(45, 'an', 'banana', 'hamster'),
(46, 'my', 'chicken', 'dog'),
(47, 'ly', 'chicken', 'dog'),
(48, 'han', 'banana', 'hamster'),
(49, 'muoi', 'chocolate', 'cat'),
(50, 'duon', 'chicken', 'dog');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
